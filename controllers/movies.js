const express = require('express');
const Movie = require('../models/movie');

const list = (req, res, next) => {

    Movie.find().then(objs => res.status(200).json({
        message: "Lista de miembros del sistema.",
        obj: objs
    })).catch(ex => res.status(500).json({
        message: "No se pudo consultar la información de los miembros.",
        obj: ex
    }));
}

const index = (req, res, next) => {

    const id = req.params.id;
    Movie.findOne({ "_id": id }).then(obj => res.status(200).json({
        message: `Movie almacenado con ID ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        message: `No se pudo consultar la información de la movie con ID ${id}.`,
        obj: ex
    }));
}

const create = (req, res, next) => {

    let genre = req.body.genre;
    let title = req.body.title;
    let director = [req.body.directorName, req.body.directorLastName];
    let actors = [req.body.actorName, req.body.actorLastName];

    let movie = new Movie({
        _genre: genre,
        _title: title,
        _director: director,
        _actors: actors
    });

    movie.save().then(obj => res.status(200).json({
        message: 'Movie creada correctamente.',
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo crear la movie",
        obj: ex
    }));
}

const replace = (req, res, next) => {

    const id = req.params.id;

    let genre = req.body.genre ? req.body.genre : "";
    let title = req.body.title ? req.body.title : "";
    let dirctor = req.body.director ? req.body.director : "";
    let actors = req.body.actors ? req.body.actors : "";

    let movie = new Object({
        _genre: genre,
        _title: title,
        _director: director,
        _actors: actors
    });

    Movie.findOneAndUpdate({ "_id": id }, movie).then(obj => res.status(200).json({
        message: "Movie remplazada correctamente.",
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo remplazar la movie.",
        obj: ex
    }));
}

const edit = (req, res, next) => {

    let genre = req.body.genre;
    let title = req.body.title;
    let director = [req.body.directorName, req.body.directorLastName];
    let actors = [req.body.actorName, req.body.actorLastName];

    let movie = new Object();

    if (genre) movie._genre = genre;
    if (title) movie._title = title;
    if (director) movie._director = director;
    if (actors) movie._actors = actors;

    console.log(movie);

    Movie.findOneAndUpdate({ "_id": id }, movie).then(obj => res.status(200).json({
        message: "Movie actualizada correctamente.",
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo actualizar la movie.",
        obj: ex
    }));
}

const destroy = (req, res, next) => {

    const id = req.params.id;
    Movie.remove({ "_id": id }).then(obj => res.status(200).json({
        message: "Movie eliminado correctamente.",
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo eliminar la movie.",
        obj: ex
    }));
}

module.exports = {
    list,
    index,
    create,
    replace,
    edit,
    destroy
};