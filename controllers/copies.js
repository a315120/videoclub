const express = require('express');
const Movie = require('../models/copie');

const list = (req, res, next) => {
    Movie.find().then(objs => res.status(200).json({
        message: "Lista de copias del sistema.",
        obj: objs
    })).catch(ex => res.status(500).json({
        message: "No se pudo consultar la información de las copias.",
        obj: ex
    }));
}

const index = (req, res, next) => {
    const id = req.params.id;
    Movie.findOne({ "_id": id }).then(obj => res.status(200).json({
        message: `Copia almacenada con ID ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        message: `No se pudo consultar la información de la copia con ID ${id}.`,
        obj: ex
    }));
}

const create = (req, res, next) => {

    let format = req.body.format;
    let movie = req.body.movie;
    let number = req.body.number;
    let status = req.body.status;

    let copie = new Copie({
        _format: format,
        _movie: movie,
        _number: number,
        _status: status
    });

    movie.save().then(obj => res.status(200).json({
        message: 'Copia creada correctamente.',
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo crear la copia",
        obj: ex
    }));
}

const replace = (req, res, next) => {
    const id = req.params.id;

    let format = req.body.format ? req.body.format : "";
    let movie = req.body.movie ? req.body.movie : "";
    let number = req.body.number ? req.body.number : "";
    let status = req.body.status ? req.body.status : "";

    let copie = new Object({
        _format: format,
        _movie: movie,
        _number: number,
        _status: status
    });

    Movie.findOneAndUpdate({ "_id": id }, copie).then(obj => res.status(200).json({
        message: "Copia remplazada correctamente.",
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo remplazar la copia.",
        obj: ex
    }));
}

const edit = (req, res, next) => {

    let id = req.params.id;

    let format = req.body.format;
    let movie = req.body.movie;
    let number = req.body.number;
    let status = req.body.status;

    let copie = new Object();

    if (format) copie._format = format;
    if (movie) copie._movie = movie;
    if (number) copie._number = number;
    if (status) copie._status = status;

    console.log(copie);

    Movie.findOneAndUpdate({ "_id": id }, copie).then(obj => res.status(200).json({
        message: "Copia actualizada correctamente.",
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo actualizar la copia.",
        obj: ex
    }));
}

const destroy = (req, res, next) => {
    const id = req.params.id;
    Movie.remove({ "_id": id }).then(obj => res.status(200).json({
        message: "Copia eliminado correctamente.",
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo eliminar la copia.",
        obj: ex
    }));
}

module.exports = {
    list,
    index,
    create,
    replace,
    edit,
    destroy
}