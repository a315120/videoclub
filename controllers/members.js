const express = require('express');
const Member = require('../models/member');

const list = (req, res, next) => {

    Member.find().then(objs => res.status(200).json({
        message: "Lista de miembros del sistema.",
        obj: objs
    })).catch(ex => res.status(500).json({
        message: "No se pudo consultar la información de los miembros.",
        obj: ex
    }));
}

const index = (req, res, next) => {

    const id = req.params.id;
    Member.findOne({ "_id": id }).then(obj => res.status(200).json({
        message: `Member almacenado con ID ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        message: `No se pudo consultar la información del member con ID ${id}.`,
        obj: ex
    }));
}

const create = (req, res, next) => {

    let city = req.body.city;
    let country = req.body.country;
    let number = req.body.number;
    let state = req.body.state;
    let street = req.body.street;

    let address = {
        city: city,
        country: country,
        number: number,
        state: state,
        street: street
    };

    let name = req.body.name;
    let lastName = req.body.lastName;
    let phone = req.body.phone;
    let status = req.body.status;

    let member = new Member({
        _address: address,
        _name: name,
        _lastName: lastName,
        _phone: phone,
        _status: status
    });

    member.save().then(obj => res.status(200).json({
        message: 'Member creado correctamente.',
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo crear el member",
        obj: ex
    }));
}

const replace = (req, res, next) => {

    const id = req.params.id;

    let city = req.body.city ? req.body.city : "";
    let country = req.body.country ? req.body.country : "";
    let number = req.body.number ? req.body.number : "";
    let state = req.body.state ? req.body.state : "";
    let street = req.body.street ? req.body.street : "";

    let address = {
        city: city,
        country: country,
        number: number,
        state: state,
        street: street
    };

    let name = req.body.name ? req.body.name : "";
    let lastName = req.body.lastName ? req.body.lastName : "";
    let phone = req.body.phone ? req.body.phone : "";
    let status = req.body.status ? req.body.status : "";

    let member = new Object({
        _address: address,
        _name: name,
        _lastName: lastName,
        _phone: phone,
        _status: status
    });

    Member.findOneAndUpdate({ "_id": id }, member).then(obj => res.status(200).json({
        message: "Member remplazado correctamente.",
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo remplazar el member.",
        obj: ex
    }));
}

const edit = (req, res, next) => {

    let city = req.body.city;
    let country = req.body.country;
    let number = req.body.number;
    let state = req.body.state;
    let street = req.body.street;

    let address = {
        city: city,
        country: country,
        number: number,
        state: state,
        street: street
    };

    let name = req.body.name;
    let lastName = req.body.lastName;
    let phone = req.body.phone;
    let status = req.body.status;

    let member = new Object();

    if (address) member._address = address;
    if (name) member._name = name;
    if (lastName) member._lastName = lastName;
    if (phone) member._phone = phone;
    if (status) member._status = status;

    console.log(member);

    Member.findOneAndUpdate({ "_id": id }, member).then(obj => res.status(200).json({
        message: "Member actualizado correctamente.",
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo actualizar el member.",
        obj: ex
    }));
}

const destroy = (req, res, next) => {

    const id = req.params.id;
    Member.remove({ "_id": id }).then(obj => res.status(200).json({
        message: "Member eliminado correctamente.",
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo eliminar el member.",
        obj: ex
    }));
}

module.exports = {
    list,
    index,
    create,
    replace,
    edit,
    destroy
};