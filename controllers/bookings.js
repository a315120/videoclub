const express = require('express');
const Booking = require('../models/booking');

const list = (req, res, next) => {
    Booking.find().then(objs => res.status(200).json({
        message: "Lista de bookinges del sistema.",
        obj: objs
    })).catch(ex => res.status(500).json({
        message: "No se pudo consultar la información de los bookinges.",
        obj: ex
    }));
}

const index = (req, res, next) => {
    const id = req.params.id;
    Booking.findOne({ "_id": id }).then(obj => res.status(200).json({
        message: `Booking almacenado con ID ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        message: `No se pudo consultar la información del booking con ID ${id}.`,
        obj: ex
    }));
}

const create = (req, res, next) => {

    let copy = req.body.copy;
    let member = req.body.member;
    let date = req.body.date;

    let booking = new Booking({
        _copy: copy,
        _memebr: member,
        _date: date
    });

    booking.save().then(obj => res.status(200).json({
        message: 'Booking creado correctamente.',
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo crear el booking",
        obj: ex
    }));
}

const replace = (req, res, next) => {

    const id = req.params.id;

    let copy = req.body.copy ? req.body.copy : ""
    let member = req.body.memebr ? req.body.member : "";
    let date = req.body.date ? req.body.date : "";

    let booking = new Object({
        _copy: copy,
        _memebr: member,
        _date: date
    });

    Booking.findOneAndUpdate({ "_id": id }, booking).then(obj => res.status(200).json({
        message: "Booking remplazado correctamente.",
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo remplazar el booking.",
        obj: ex
    }));
}

const edit = (req, res, next) => {

    let id = req.params.id;

    let copy = req.body.copy;
    let memebr = req.body.memebr;
    let date = req.body.date;

    let booking = new Object();

    if (copy) booking._copy = copy;
    if (memebr) booking._member = memebr;
    if (date) booking._date = date;

    console.log(booking);

    Booking.findOneAndUpdate({ "_id": id }, booking).then(obj => res.status(200).json({
        message: "Booking actualizado correctamente.",
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo actualizar el booking.",
        obj: ex
    }));
}

const destroy = (req, res, next) => {
    const id = req.params.id;
    Booking.remove({ "_id": id }).then(obj => res.status(200).json({
        message: "Booking eliminado correctamente.",
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo eliminar el booking.",
        obj: ex
    }));
}

module.exports = {
    list,
    index,
    create,
    replace,
    edit,
    destroy
}