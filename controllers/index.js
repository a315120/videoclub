const express = require('express');
const bcrypt = require('bcrypt');
const async = require('async');
const jwt = require('jsonwebtoken');
const User = require('../models/user');

const jwtKey = "5ab81308a12fba639b253fecd48b140b";

const home = (req,res,next) => {
    res.render('index', {title: 'Express'});
}

const login = (req,res,next) => {
    let email =req.body.email;
    let password = req.body.password;
    console.log(email);

    async.parallel({
        user: callback => User.findOne({ _email : email })
                              .select('_password _salt') //nunca pidas mas informacion de la que necesitas
                              .exec(callback) 
    }, (err, result)=>{
        console.log(result.user);
        if(result.user){
            bcrypt.hash(password, result.user.salt, (err, hash) => { 
                console.log(hash);
                console.log(result.user._password);
                if(hash === result.user._password){
                    res.status(200).json({
                        "message":"Usuario y contrasena ok",
                        "obj": jwt.sign(result.user.id, jwtKey)
                    });
                }else{
                    res.status(403).json({
                        "message":"Usuario y/o contrasena incorrectos 2.", obj:null
                    });
                }
            });
        }else{
            res.status(403).json({
                "message":"Usuario y/o contrasena incorrectos.", obj:null
            });
        }

    })
}


module.exports = {
    home, login
}