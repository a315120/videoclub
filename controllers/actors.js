const express = require('express');
const Actor = require('../models/actor');

const list = (req,res,next) => {
    Actor.find().then(objs => res.status(200).json({
        message: "Lista de actores del sistema.",
        obj: objs
    })).catch(ex => res.status(500).json({
        message: "No se pudo consultar la información de los actores.",
        obj: ex
    }));
}

const index = (req,res,next) => {
    const id = req.params.id;
    Actor.findOne({"_id":id}).then(obj => res.status(200).json({
        message: `Actor almacenado con ID ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        message: `No se pudo consultar la información del actor con ID ${id}.`,
        obj: ex
    }));
}

const create = (req,res,next) => {
    let name = req.body.name;
    let lastName = req.body.lastName;

    let actor = new Actor({
        _name:name,
        _lastName:lastName
    });

    actor.save().then(obj => res.status(200).json({
        message: 'Actor creado correctamente.',
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo crear el actor",
        obj: ex
    }));
}

const replace = (req,res,next) => {
    const id = req.params.id;
    let name = req.body.name ? req.body.name : "";
    let lastName = req.body.lastName ? req.body.lastName : "";

    let actor = new Object({
        _name: name,
        _lastName: lastName
    });

    Actor.findOneAndUpdate({"_id":id},actor).then(obj => res.status(200).json({
        message: "Actor remplazado correctamente.",
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo remplazar el actor.",
        obj: ex
    }));
}

const edit = (req,res,next) => {
    let id = req.params.id;
    let name = req.body.name;
    let lastName = req.body.lastName;
    
    let actor = new Object();

    if(name) actor._name = name;
    if(lastName) actor._lastName = lastName;

    console.log(actor);

    Actor.findOneAndUpdate({"_id":id},actor).then(obj => res.status(200).json({
        message: "Actor actualizado correctamente.",
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo actualizar el actor.",
        obj: ex
    }));
}

const destroy = (req,res,next) => {
    const id = req.params.id;
    Actor.remove({"_id":id}).then(obj => res.status(200).json({
        message: "Actor eliminado correctamente.",
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo eliminar el actor.",
        obj: ex
    }));
}

module.exports = {
    list, index, create, replace, edit, destroy
}