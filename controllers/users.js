const express = require('express');
const bcrypt = require('bcrypt');
const async = require ('async');
const mongoosePaginate = require('mongoose-paginate-v2');

const User = require ('../models/user');


function list(req, res, next) {
    let page = req.params.page ? req.params.page : 1;
    User.paginate({}, {page:page, limit:3}).then(objs => res.status(200).json({
        message: "Lista de actores del sistema",
        obj: objs
    })).catch(ex => res.status(500).json({
        message: "No se pudo consultar la info de los actores",
        obj: ex
    }));
}

function index(req,res,next){   
    const id =req.params.id;
   User.findOne({"_id":id}).then(obj => res.status(200).json({
       message: `usuario almacenado con ID ${id}`,
       obj: obj
   })).catch(ex => res.status(500).json({
        message: `No se pudo consultar la info del usuario con id ${id}`,
        obj: ex
    }));
}

function create(req,res,next){
    const email = req.body.email;
    const name = req.body.name;//implicitos o sobre el cuerpo
    const lastName = req.body.lastName;
    const password =req.body.password;
    

    async.parallel({
        salt: (callback) =>{
            bcrypt.genSalt(10, callback);
        }
    }, (err, result) =>{
       
        bcrypt.hash(password, result.salt, (err, hash)=>{
            let user = new User({
                _email:email,
                _name: name,
                _lastName: lastName,
                _password: hash,
                _salt: result.salt
            });
           
            user.save().then(obj => res.status(200).json({
                message : 'user creado correctamente',
                obj: obj
            })).catch(ex =>res.status(500).json({
                message: 'No se pudo almacena el user',
                obj: ex
            }));
        });
    });
}

function replace(req,res,next){
    const id = req.params.id;
    let email = req.body.email ? req.body.email: "";
    let name = req.body.name ? req.body.name: "";
    let lastName = req.body.lastName ? req.body.lastName: "";
    let password = req.body.password ? req.body.password: "";

    let user = new Object({
        _email: email,
        _name: name,
        _lastName : lastName,
        _password : password
    });

    User.findOneAndUpdate({"_id":id},user).then(obj => res.status(200).json({
        message: "user remplazado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message: `No se pudo remplazar el user con id ${id}`,
        obj: ex
    }));
}

function edit(req,res,next){
    const id = req.params.id;
    let email = req.body.email;
    let name = req.body.name;
    let lastName = req.body.lastName;
    let password = req.body.password;

    let user = new Object();

    if(email){
        user._email = email;
    }

    if(name){
        user._name = name;
    }

    if(lastName){
        user._lastName = lastName;
    }

    if(password){
        user._password =password;
    }

   User.findOneAndUpdate({"_id":id},user).then(obj => res.status(200).json({
        message: "user Actualizado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message: `No se pudo actualizar el user con id ${id}`,
        obj: ex
    }));
}

function destroy(req,res,next){
    const id = req.params.id;
    User.remove({"_id":id}).then(obj => res.status(200).json({
        message: "User eliminado correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message: `No se pudo eliminar el user con id ${id}`,
        obj: ex
    }));
}

module.exports ={
    list, index,create,edit,replace,destroy
}