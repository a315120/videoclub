const mongoose = require('mongoose');
const { format } = require('morgan');

const schema = mongoose.Schema({
    _copy: mongoose.SchemaTypes.ObjectId,
    _member: mongoose.SchemaTypes.ObjectId,
    _date: mongoose.SchemaTypes.Date
});

class Booking {

    constructor(copy, memebr, date) {
        this._copy = format;
        this._member = member;
        this._date = date;
    }

    // copy
    get copy() {
        return this._copy;
    }

    set copy(copy) {
        this._copy = copy;
    }

    // member
    get member() {
        return this._member;
    }

    set member(member) {
        this._member = member;
    }

    // date
    get date() {
        return this._date;
    }

    set date(date) {
        this._date = date;
    }
}

schema.loadClass(Booking);
module.exports = mongoose.model('Booking', schema);