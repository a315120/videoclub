const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _address: {
        city: String,
        country: String,
        number: String,
        state: String,
        street: String
    },
    _lastName: String,
    _name: String,
    _phone: String,
    _status: Boolean,
});

class Member {

    constructor(address, name, lastName, phone, status) {
        this._address = address;
        this._name = name;
        this._lastName = lastName;
        this._phone = phone;
        this._status = status;
    }

    // Address
    get address() {
        return this._address;
    }

    set address(address) {
        this._address = address;
    }

    // name
    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name
    }

    // lastName
    get lastName() {
        return this._lastName;
    }

    set lastName(lastName) {
        this._lastName = lastName;
    }

    // phone
    get phone() {
        return this._phone;
    }

    set phone(phone) {
        this._phone = phone;
    }

    // status
    get status() {
        return this._status;
    }

    set status(status) {
        this._status = status;
    }
}

schema.loadClass(Member);
module.exports = mongoose.model('Member', schema);