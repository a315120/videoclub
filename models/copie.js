const mongoose = require('mongoose');
const { format } = require('morgan');

const schema = mongoose.Schema({
    _format: String,
    _movie: mongoose.SchemaTypes.ObjectId,
    _number: mongoose.SchemaTypes.Number,
    _status: String
});

class Copie {

    constructor(format, movie, number, status) {
        this._format = format;
        this._movie = movie;
        this._number = number;
        this._status = status;
    }

    // format
    get format() {
        return this._format;
    }

    set format(format) {
        this._format = format;
    }

    // movie
    get movie() {
        return this._movie;
    }

    set movie(movie) {
        this._movie = movie;
    }

    // number
    get actors() {
        return this._number;
    }

    set number(number) {
        this._number = number;
    }
}

schema.loadClass(Copie);
module.exports = mongoose.model('Copie', schema);