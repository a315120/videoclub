const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
    _email: String,
    _name: String,
    _lastName: String,
    _password: String,
    _salt: String
});

class User {

    constructor(email, name, lastName, password, salt) {
        this._email = email;
        this._name = name;
        this._lastName = lastName;
        this._password = password;
        this._salt = salt
    }

    // email
    get email() {
        return this._email;
    }

    set email(email) {
        this._email = email;
    }

    // name
    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name;
    }

    // lastname
    get lastName() {
        return this._lastName;
    }

    set lastName(lastName) {
        this._lastName = this.lastName
    }

    // password
    get password() {
        return this._passwrod;
    }

    set password(passwrod) {
        this._password = passwrod;
    }

    // salt
    get salt() {
        return this._salt;
    }

    set password(salt) {
        this._salt = salt;
    }
}

schema.loadClass(User);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('User', schema);