const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _genre: String,
    _title: String,
    _director: {
        _name: String,
        _lastName: String
    },
    _actors: {
        _name: String,
        _lastName: String
    }
});

class Movie {

    constructor(genre, title, director, actors) {
        this._genre = genre;
        this._title = title;
        this._director = director;
        this._actors = actors;
    }

    // genre
    get address() {
        return this._genre;
    }

    set address(genre) {
        this._genre = genre;
    }

    // title
    get title() {
        return this._title;
    }

    set director(director) {
        this._director = director;
    }

    // actors
    get actors() {
        return this._actors;
    }

    set actors(actors) {
        this._actors = actors;
    }
}

schema.loadClass(Movie);
module.exports = mongoose.model('Movie', schema);